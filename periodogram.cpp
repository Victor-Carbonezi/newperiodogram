#include "periodogram.h"

Periodogram::Periodogram()
{
}

void Periodogram::calculate(QVector<double> tallies, QVector<double> pArray, double alpha, int step, bool showData, bool showResult)
{
  int n = tallies.length();
  if(n < 2)
    {
      qDebug() << "O vetor necessita de pelo menos 2 elementos";
      return;
    }

  if(pArray.length() == 0)
    {
      pArray = linspace(14*60,34*60,1);
    }

  int p;
  QVector<double> output;
  for(p = 0; p < pArray.length(); p++)
    {
      int P = qFloor(pArray.at(p));
      int K = qFloor(n/P);

      QVector<double> cMeans;
      for(int j = 0; j < P; j++)
        {
          QVector<double> index;
          for(int k = 0; k < K; k++)
            {
              int aux = qCeil(j + k*pArray.at(p));
              if(aux < n)
                {
                  index.append(aux);
                }
            }
          double sum = 0;
          for(int i = 0; i < index.length(); i++)
            {
              sum += tallies.at(index.at(i));
            }
          cMeans.append(sum/K);
          index.clear();
        }
      double tMean = sum(cMeans)/pArray.at(p);
      QVector<double> cMinust;
      QVector<double> talliesMinust;
      for(int i = 0; i < cMeans.length(); i++)
        {
          cMinust.append(cMeans.at(i) - tMean);
        }
      for(int i = 0; i < tallies.length(); i++)
        {
          talliesMinust.append(tallies.at(i) - tMean);
          //qDebug() << "talliesMinust = " << talliesMinust.at(i);
        }

      double A2 = sumWithExponential(cMinust ,2)/P;

      double meanVar = sumWithExponential(talliesMinust, 2)/(n*K);
      //qDebug() <<"meanVar = " << meanVar;


      double Q = P*A2/meanVar;
      output.append(Q);
      cMeans.clear();

      //qDebug() << "Q = " << Q ;

    }
  double peak = max(output);
  qDebug() << "peak = " << peak;
  qDebug() << "length = " << pArray.length() << output.length();
  double period = pArray.at(output.indexOf(peak));

  qDebug() << "Peak, period = " << peak << period;
}

/**
 * @brief Cosinor::linspace, creates a double vector with num_in values equally espaced
 * @param start_in: start point
 * @param end_in: end point
 * @param num_in:step size
 * @return The double vector with num_in values starting in start_in and ending in end_in
 */
QVector<double> Periodogram::linspace(double start_in, double end_in, double num_in)
{

  QVector<double> linspaced;

  /*double start = start_in;
  double end = end_in;
  double num = num_in;

  if (num == 0) { return linspaced; }
  if (num == 1)
    {
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1);

  for(int i=0; i < num-1; ++i)
    {
      linspaced.push_back(start + delta * i);
    }
  linspaced.push_back(end);*/ // I want to ensure that start and end
                            // are exactly the same as the input
  linspaced.append(start_in);

  for(int i = 1; start_in + i*num_in < end_in; i++)
    {
      linspaced.append(start_in + i*num_in);
    }

  return linspaced;
}

/**
 * @brief Sum all the values of a vector
 * @param vector - Double QVector
 * @return The sum of all values
 */
double Periodogram::sum(QVector<double> vector)
{
    double sum = 0;
    for(int index = 0; index < vector.length(); index++)
    {
        sum += vector.at(index);
    }
    return sum;
}

double Periodogram::sumWithMultiplication(QVector<double> vector1, QVector<double> vector2)
{
    if( vector1.length() != vector2.length() || vector1.isEmpty() )
    {
        return 0.0;
    }

    double sum = 0;
    for(int index = 0; index < vector1.length(); index++)
    {
        sum += vector1.at(index) * vector2.at(index);
    }
    return sum;
}

/**
 * @brief Sum with exponential.
 *
 * For example:
 * for(int index = 0; index < vector.length(); index++)
 *     sum += pow( vector.at(index), 2 );
 *
 * @param vector - Double QVector
 * @param power - The power of the exponential
 * @return The total sum or zero if the vector is empty
 */
double Periodogram::sumWithExponential(QVector<double> vector, double power)
{
    if( vector.isEmpty() || power == 0 )
    {
        return 0.0;
    }

    double sum = 0;
    for(int index = 0; index < vector.length(); index++)
    {
        sum += pow( vector.at(index), power );
    }
    return sum;
}

double Periodogram::max(QVector<double> vector)
{
  double max = vector.at(0);

  for(int i = 1; i < vector.length() ; i++)
    {
      if(vector.at(i) > max)
        {
          max = vector.at(i);
        }
    }

  return max;
}


