#include "mainwindow.h"
#include "periodogramtest.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  PeriodogramTest *periodogramTest_ = new PeriodogramTest();
  w.show();
  return a.exec();
}
