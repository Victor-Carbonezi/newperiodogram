#ifndef PERIODOGRAM_H
#define PERIODOGRAM_H

#include <QVector>
#include <QtMath>
#include <cmath>
#include <algorithm>
#include <iostream>

#include <QDebug>


class Periodogram
{
public:
  Periodogram();
  void calculate(QVector<double>,QVector<double>, double alpha = 0.001, int step = 20, bool showData = false, bool showResult = false);
  QVector<double> linspace(double start_in, double end_in, double num_in);
  double sum(QVector<double> vector);
  double sumWithMultiplication(QVector<double> vector1, QVector<double> vector2);
  double sumWithExponential(QVector<double> vector, double pow);
  double max(QVector<double> vector);
};

#endif // PERIODOGRAM_H
